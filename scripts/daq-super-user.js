
// Start DAQ in "super_user" mode, mainly used to benchmark and test

const dim = require('dim');

const DAQ = 'ntofdaq-m12';
const DNS = 'ntofdaq-m12.cern.ch:2505';

async function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitState(service, stateStr) {
  for (;;) {
    const ret = await dim.DicXmlState.get(service, DNS, 5000);
    if (ret && ret.strValue === stateStr) {
      break;
    }
    else {
      console.log(`waiting for state "${stateStr}" current:"${ ret ? ret.strValue : 'unknown' }"`);
      await delay(1000);
    }
  }
}


(async function() {
  await waitState(`${DAQ}/DaqState`, 'Not configured');
  await dim.DicXmlCmd.invoke(`${DAQ}/Daq/Command`,
    { command: { command: 'super_user' } }, DNS)

  await waitState(`${DAQ}/DaqState`, 'Waiting for command');
  await dim.DicXmlCmd.invoke(`${DAQ}/Daq/Command`,
    { command: { command: 'initialization' } }, DNS)

  await waitState(`${DAQ}/DaqState`, 'Waiting for start acquisition command');
  await dim.DicXmlCmd.invoke(`${DAQ}/Daq/Command`,
    { command: { command: 'start' } }, DNS)
})()
.catch((e) => console.log(e));

