
// monitor RawFileMerger info service

const dim = require('dim');

const DAQ = 'ntofdaq-m12';
const DNS = 'ntofdaq-m12.cern.ch:2505';

async function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

(async function() {
  let val = new dim.DicValue('MERGER/Info', null, DNS);
  val.on('value', (v) => console.log(v));
  console.log(await val.promise());
})()
.catch((e) => console.log(e));

