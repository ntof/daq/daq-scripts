// enable/disable timing machine
// check logs to ensure proper information is retrieved

const
  prom = require('prom'),
  { DicValue } = require('dim'),
  { DicXmlParams, XmlData } = require('dim-xml');

const DNS = 'ntofdaq-m12.cern.ch:2505';

const TimingParamsIdx = {
  MODE: 1,
  TRIGGER_REPEAT: 2,
  TRIGGER_PERIOD: 3,
  TRIGGER_PAUSE: 4,
  EVENT_NUMBER: 5
};

const TimingMode = {
  DISABLED: 0,
  AUTOMATIC: 1,
  CALIBRATION: 2
};

let events = new DicValue('Timing/event', null, DNS);
let params = new DicXmlParams('Timing', null, DNS);

(async function() {
  let accu = [];
  events.on('value', (v) => {
    accu.push(v);
    console.log('timing value :', v)
  });
  console.log('timing event :', await events.promise());

  /* wait for everything to be connected */
  await params.promise();
  await prom.delay(1500);

  accu = [];

  console.log('setting calibration mode');
  await params.setParams([
    { index: TimingParamsIdx.MODE, type: XmlData.Type.ENUM, value: TimingMode.CALIBRATION },
    { index: TimingParamsIdx.TRIGGER_PERIOD, type: XmlData.Type.INT32, value: 500 },
    // setting EVENT_NUMBER last, was creating a bug
    { index: TimingParamsIdx.EVENT_NUMBER, type: XmlData.Type.INT64, value: 0 }
  ]);
  accu = [];
  console.log('mode: calibration');
  await prom.delay(1500);

  await params.setParams({ index: TimingParamsIdx.MODE, type: XmlData.Type.ENUM, value: TimingMode.DISABLED });
  console.log('mode: disabled');

  await prom.delay(1500);
  
  // hammer some mode changes 
  for (let i = 0; i < 1500; ++i)
  {
    await params.setParams([
      { index: TimingParamsIdx.MODE, type: XmlData.Type.ENUM, value: TimingMode.CALIBRATION },
      { index: TimingParamsIdx.TRIGGER_PERIOD, type: XmlData.Type.INT32, value: 500 }
    ]);
  }

  console.log('events received after reset:', accu);
})()
.finally(() => {
  events.release();
  params.release();
})
.catch((e) => console.log(e));

