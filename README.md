
# DAQ scripts

Small scripts to test and manipulate n_ToF DAQ

Those scripts are just there for convenience and to ease debugging, it is **not** suitable for operation.

# Usage

```bash
# Install dependencies:
npm install

# Run scripts:
node ./scripts/timing-mode-change.js
```